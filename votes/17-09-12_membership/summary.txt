=====================================================================
Summary
=====================================================================

Topic: Membership
Date taken: 17/8/29 - 17/9/12
Vote count: 36 (of 92 eligible voters, 39%)

Secretary: Karsten Loesing
Proposer: Alison Macrina

Topic put to a vote were...

  Question 1: Social Contract Proposal

    There are no alternate proposals for these guidelines, so please choose
    A or B.

    A. I approve of the attached proposal.
    B. I reject the attached proposal.

Results were...

  * Proposal passes. 89% approved of its adoption. We'll institute
    this as our membership policy.

=====================================================================
Votes
=====================================================================

Adam Shostack
Alison Macrina
Allen Gunn
Chelsea
Colin Childs
Damian Johnson
David Goulet
Donncha O'Cearbhaill
Georg Koppen
Hiro
Ian Goldberg
Isabela
Isis
Jens Kubieziel
Julius Mittenzwei
Juris
Kat
Linda Lee
Linus Nordberg
Maria Xynou
Mark Smith
Matt Traudt
Matthew Finkel
Moritz Bartl
Nick Mathewson
Nicolas Vigier
Paul Syverson
Rob Jansen
Roger Dingledine
Steven Murdoch
Sukhbir Singh
Taylor Yu
teor
Tom Ritter
Tommy Collison
Yawning Angel

1341;A
2775;A
4133;A
4355;A
5502;A
6670;A
8323;A
8449;A
8809;A
10307;B
15300;A
15896;A
15923;A
15932;A
17425;A
17641;A
17650;A
17888;A
18477;A
18821;A
21619;B
21700;A
22451;B
23338;A
23340;A
23945;A
24310;A
24434;A
26703;A
27547;A
28495;A
29610;A
30884;B
31500;A
32392;A
32594;A

=====================================================================
Question 1: Membership Proposal
=====================================================================

Approve: 32 (89%)
Reject: 4 (11%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

Proposal passes. 89% approved of its adoption. We'll institute
this as our membership policy.

