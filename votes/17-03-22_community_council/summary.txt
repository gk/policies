=====================================================================
Summary
=====================================================================

Topic: Community Council
Date taken: 17/3/10 - 17/3/22
Vote count: 29 (of 83 eligible voters, 35%)

Secretary: Karsten Loesing
Proposer: Alison Macrina and Damian Johnson

Topic put to a vote were...

  Question 1: Voting Proposal

    There are no alternate proposals for these guidelines, so please
    choose A or B. Please note that within the proposal are three
    additional options to vote on. They are listed below in the voting
    instructions, but you'll also find them within the text so that you
    can understand their context.

    a. I approve of the attached proposal and have voted on the in-text
       options.

    b. I reject the attached proposal.

  Question 2: Ranking for Member Selection

    a. Please rank these individuals by your preference for them to be on
       the new Community Council.

    b. List up to five of these individuals you'd like to see on the new
       Community Council.

  Question 3: Council Size

    a. If we lack five acceptable volunteers for the new council then the
       council size is dropped to three.

    b. If we lack five acceptable volunteers for the new council then the
       council size can be dropped to four or three.

  Question 4: Diversity

    Please vote 'yes' or 'no' on the following...

    The Community Council needs to represent the breadth of the Tor
    community to function effectively. As such, the Community Council
    must include at least one female-identified person. If we lack a
    female-identified person before the councilmembers are voted on, the
    vote can be delayed up to one month and another call for volunteers
    will be sent out.

    We recognize the need for other kinds of voices that were not well
    represented in our community at the time these guidelines were
    ratified -- namely, a diverse set of racial, geographic, linguistic,
    and technical backgrounds. Since we currently lack enough current
    contributors to make representation of those identities a requirement
    for this council, we will acknowledge here the priority of getting
    better representation in our community as a whole.

Results were...

  * Proposal passes. 83% approved of its adoption. We'll institute a
    community council using these guidelines.

  * Voters won't be allowed to specify an ordered preference (57%).

  * If we lack five acceptable volunteers the council size is dropped
    to four, followed by three. (79%)

  * A council cannot be formed without a female-identified person
    included (78%).

=====================================================================
Votes
=====================================================================

Adam Shostack
Alison
Allen Gunn
Arthur D. Edelstein
Damian Johnson
David Goulet
isis agora lovecruft
Jens Kubieziel
Joshua Gay
Leif Ryge
Linus Nordberg
Matthew Finkel
Meredith Hoban Dunn
micah
Mike Perry
Moritz Bartl
Nick Mathewson
Nicolas Vigier
Nima Fatemi
Paul Syverson
Philipp Winter
Rob Jansen
Roger Dingledine
Silvia [Hiro]
Steven Murdoch
Sue Gardner
teor
Tom Ritter
Yawning Angel

267; A; B; A; yes
803; B; <abstain>; <abstain>; <abstain>
4206; A; A; A; yes
4585; A; B; A; yes
5837; A; A; B; yes
6873; A; B; B; yes
8061; A; A; B; no
9684; A; B; B; yes
12199; A; A; B; yes
13437, A; <none>, B, A; B; yes
13803; B; B; B; no
16316; A; A; B; <abstain>
16980; A; <none>, B; B; yes;
17875; A; A; B; no
19066; A; B; B; yes
21068; A; B; B; yes
21368; A; A; A; yes
22706; B; B; B; yes
23505; A; A; B; yes
24488; A; B; B; yes
25377; A; A; B; yes
27624; A; A; A; no
28808; B; B; B; no
29274; A; B; B; yes
30378; A; B; B; yes
30913; B; B; A; no
31790; A; B; B; yes
31879; A; A; B; yes
32103; A; A; B; yes

=====================================================================
Question 1: Voting Proposal
=====================================================================

Approve: 24 (83%)
Reject: 5 (17%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

Motion passes. We'll institute a community council using these
guidelines.

=====================================================================
Question 2: Ranking for Member Selection
=====================================================================

Option a (ranked vote): 12 (43%)
Option b (no ranking): 16 (57%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

Voters won't be allowed to specify an ordered preference.

=====================================================================
Question 3: Council Size
=====================================================================

Option a (drop to three): 6 (21%)
Option b (drop to four, then three): 22 (79%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

If we lack five acceptable volunteers the council size is dropped to
four, followed by three.

=====================================================================
Question 4: Diversity
=====================================================================

Option a (require a female council member): 21 (78%)
Option b (don't require a female council member): 6 (22%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

A council cannot be formed without a female-identified person included.

