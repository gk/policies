```
Filename: 001-community-relay-operator-process.md
Title: Process for new policies and proposals for the Tor relay operator community
Author: gus - gus@torproject.org
Created: 2023-05-22
Status: Meta
```

## Overview

This document outlines the process for submitting policies to the Tor relay operator community, explains how proposals are approved, and clarifies the relationship between Tor Community proposals and the Tor Project. This is an informational document.

## Motivation

In the past, our approach to organizing the Tor relay operator community was highly informal and relied heavily on the informal dynamics between relay operators, Directory Authorities, and The Tor Project staff. Through discussions and interactions in meetups and similar events, operators would collaboratively determine the best practices for managing Tor relays while ensuring the well-being of Tor users. However, as the Tor Community expands and its members become less interconnected, there is now a necessity to establish a well-defined process for presenting and evaluating proposals and policies to the relay operator community.

It's important to note that these proposals are distinct from the Tor proposals governed by torspec. The proposals in tor spec primarily focus on technical specifications and protocols at the core of the Tor network. They cover areas such as the Tor protocol, directory authorities, circuit construction, and encryption mechanisms.

The proposals in this document, on the other hand, are specifically tailored to the relay operator community and address operational, policy, and community-related aspects. While both types of proposals aim to improve the Tor network, they serve different purposes and operate within their respective scopes.

It's part of the scope of these proposals addressing topics like:
- Security measures to combat malicious relays and attacks against the Tor network like DDoS
- Expectations for relays operators and operational policies
- Sustainability and operators incentivisation
- Guidelines for conducting investigations to identify and remove malicious actors from the Tor network
- Initiatives related to relay operator governance and community building

There might be corner cases where proposals impacting the relay operator community would contain torspec material if approved. In that case it is fine to submit a proposal within this community proposal process first and a corresponding torspec on later one if the proposal got approved.

## How new proposals get added, approved and implemented

The process of adding, approving, and implementing new proposals for the relay operator community follows a specific workflow as described below.

Here is an overview of the steps involved:

Summary of the stages:
    Draft & Full Proposal -> Policy -> Implementation

The current proposal editors are the Tor Project Network Health and Community teams leads, who can reject or accept full proposals. The criteria for evaluation of proposals for relay operators are stated on the document ["Combating Malicious Relays - Evaluation criteria for solutions"](https://nc.torproject.net/s/j2EzrqLBtYwYxxf).

### 1. Draft, consensus, and full proposal

To submit a proposal, open directly a new issue in the Tor Project GitLab [Community/Policies repository](https://gitlab.torproject.org/tpo/community/policies).

A proposal should have a properly formatted (see below) draft. Once an idea moves to this stage, the Tor Community should discuss and improve it until we've reached consensus that it's a good idea and that it's detailed enough to implement.

The official request for changes, suggestions and improvements of the proposal must happen in the GitLab comment section so that the author can directly receive the feedback.

To submit a new update of the proposal, the author should add a comment with the new alternative text in the appropriate ticket. Additionally, the author must update the issue description to indicate the availability of a new version of the proposal. This ensures that the proposal is properly documented.

### 2. Policy and implementation

Once the full proposal has reached a consensus and its final version was approved by the proposal editors, they will officially create a merge request and add the approved proposal. When this happens, we incorporate it into the Relay Operator policies directory and implement the proposal.

This process ensures that proposals undergo thorough discussion and consensus-building, helping maintain the integrity of The Tor Project's policies and operations.

## Observations

Unlike RFCs (Request for Comments), Tor relay operator community proposals can evolve over time while retaining the same number until they are ultimately accepted or rejected. The entire history of each proposal will be archived in the Tor Community Policies repository.

It's still okay to make small changes directly to your proposal once it's finalized if it's cosmetic changes and no substantial change is required.

This document reflects the current contributors' _intent_, not a permanent promise to always use this process in the future.

Technical Tor spec proposals are out of the scope of this document and must follow the process described on [Tor specification](https://gitlab.torproject.org/tpo/core/torspec/).

If consensus cannot be reached among the Tor relay operators community during the proposal review process, a Voting process involving Core contributors can be initiated. The decision to proceed with a voting process is at the discretion of the proposal editors. They are responsible for determining whether a voting process is necessary to resolve the impasse. In such cases, the voting process must follow the [Voting policy](https://gitlab.torproject.org/tpo/community/policies/-/blob/master/voting.txt).

## Responsibilities and roles of proposal editors

The proposal editors play a crucial role in the evaluation and approval of proposals within the Tor relay operator community. Their responsibilities include:

### 1. Reviewing and evaluating proposals

The proposal editors evaluate each submitted proposal to assess its feasibility, adherence to community policies, and potential impact on the Tor network and its users.

### 2. Facilitating discussions and building consensus

The proposal editors actively participate in discussions related to proposals, engaging with the Tor relay operator community to understand different perspectives and build consensus. They facilitate constructive dialogue, encouraging community members to provide feedback, suggest improvements, and address concerns raised during the proposal review process.

### 3. Determining proposal status

The proposal editors are responsible for assigning appropriate proposal statuses. They evaluate the progress and quality of proposals, assigning statuses such as "Draft," "Open," "Needs-Revision," "Accepted," "Finished," "Closed," "Rejected," "Dead," "Meta," or "Reserve." The proposal editors maintain the proposal status throughout the review process until a final decision is reached.

### 4. Maintaining proposal integrity and documentation

The proposal editors ensure the integrity and accuracy of proposals within the Tor Community/Policies repository. They review proposals for adherence to formatting guidelines and make necessary adjustments to maintain consistency.

## Proposals made by proposal editors

When proposal editors intend to submit their own proposals, they must follow the standard process outlined in the "How new proposals get added, approved and implemented" section of this document.

However, when it comes to merging their own proposals, the proposal editors should not unilaterally merge their own proposals without proper evaluation and consensus from the Tor relay operator community. The proposal editors have the responsibility to maintain the integrity and fairness of the proposal review process.

To avoid conflicts of interest and ensure transparency, full proposals made by proposal editors must be sponsored by two other Tor core contributors.

## What should go in a proposal

Every proposal must have a header containing these fields:

    * Filename
    * Title
    * Author
    * Created
    * Status

The body of the proposal should start with an Overview section explaining what the proposal's about, what it does, and about what state it's in.

After the Overview, the proposal becomes more free-form. Depending on its length and complexity, the proposal can break into sections as appropriate, or follow a short discursive format. Every proposal should contain at least the following information before it is "ACCEPTED", though the information does not need to be in sections with these names.

Motivation: What problem is the proposal trying to solve? Why does this problem matter? If several approaches are possible, why take this one?

Security implications: What effects the proposed changes might have on anonymity, how well understood these effects are, and so on.

Implementation: If the proposal will be tricky to implement by the Tor Community, the document can contain some discussion of how to go about making it work.

Participation: How do you think your proposal will affect the existing relay operators? Additionally, do you expect new operators to join? If you expect new operators to join, what kind do you foresee?

## How to format proposals

Proposals must be written in Markdown.

## Proposal status

Draft: This isn't a complete proposal yet; there are definite missing pieces. Please don't add any new proposals with this status.

Open: A proposal under discussion.

Needs-Revision: The idea for the proposal is a good one, but the proposal as it stands has serious problems that keep it from being accepted. See comments in the ticket for details.

Accepted: The proposal is complete, and we intend to implement it. After this point, substantive changes to the proposal should be avoided, and regarded as a sign of the process having failed somewhere.

Finished: The proposal has been accepted and implemented. After this point, the proposal should not be changed.

Closed: The proposal has been accepted, implemented, and merged into the main policies documents. The proposal should not be changed after this point.

Rejected: We're not going to implement the proposal as described here, though we might do some other version. See comments in the ticket for details. The proposal should not be changed after this point; to bring up some other version of the idea, write a new proposal.

Dead: The proposal hasn't been touched in a long time, and it doesn't look like anybody is going to complete it soon. It can become "Open" again if it gets a new proponent.

Meta: This is not a proposal, but a document about proposals.

Reserve: This proposal is not something we're currently planning to implement, but we might want to resurrect it some day if we decide to do something like what it proposes.

Obsolete: This proposal was flawed and has been superseded by another proposal. See comments in the document for details.

The proposal editors maintain the correct status of proposals, based on rough consensus and their own discretion.

## Proposal numbering

Numbers 000-099 are reserved for special and meta-proposals. 100 and up are used for actual proposals. Numbers aren't recycled.
